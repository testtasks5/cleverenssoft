1) Дана строка, содеражащая n маленьких букв латинского алфавита. Требуется реализовать алгоритм компресии 
этой строки, замещающий группы последовательно идущих одинаковых букв формой 
"sc"(где "s" - символ, "c" - количество букв в группе), а также алгоритм декомпрессии, возвращающий исходную строку.