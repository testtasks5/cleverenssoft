using System;
using System.Text;
using System.Collections.Generic;

namespace CompressionService;

public class Compression
{
    public StringBuilder CompressionMethod(string str)
    {
        StringBuilder st = new StringBuilder();
        for (int i = 0; i < str.Length; i++)
        {
            int count = 1;
            int y = i + 1;
            if(y <= str.Length - 1)
            {
                while(str[i] == str[y] && y < str.Length)
                {
                    count++;
                    y++;
                    if(y >= str.Length)
                        break;
                }
            }
            if(count > 1)
                st.Append(str[i].ToString() + count.ToString());
            else
                st.Append(str[i].ToString());
            i = i + count - 1;
        }
        return st;
    }
    public StringBuilder DecompressionMethod(string str)
    {
        StringBuilder st = new StringBuilder();
        List<string> numbs = new List<string>();
        for(int i = 0; i < str.Length;)
        {
            if(Char.IsNumber(str[i]))
            {   
                for(int y = i;y < str.Length; y++)
                    if(Char.IsNumber(str[y]))
                        numbs.Add(str[y].ToString());
                    else
                        break;
            }
            else    
            {
                st.Append(str[i]);
                i++;
                numbs.Clear();
            }
            
            if(numbs.Count >= 1)
            {
                StringBuilder numb = new StringBuilder();
                foreach(var item in numbs)
                    numb.Append(item);
                string s = numb.ToString();
                int countChar = Convert.ToInt32(s);
                for (int x = 0; x < countChar - 1; x++)
                {
                    st.Append(str[i-1]);
                }
                i = i + numbs.Count;
            }
        }
        return st;
    }
}