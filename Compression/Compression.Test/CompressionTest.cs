using CompressionService;
namespace Compression.Test;

public class CompressionTest
{
    [Theory]
    [InlineData("aaabbrcccddee")]
    public void TestCompressionRuntime(string str)
    {
        CompressionService.Compression compression = new CompressionService.Compression();
        
        var comp = compression.CompressionMethod(str);
        var decomp = compression.DecompressionMethod(comp.ToString());

        Assert.Equal(str, decomp.ToString());
    }
    [Theory]
    [InlineData("aaaaaaaaaaaa")]
    public void TestCompressionRuntime2(string str)
    {
        CompressionService.Compression compression = new CompressionService.Compression();
        
        var comp = compression.CompressionMethod(str);
        var decomp = compression.DecompressionMethod(comp.ToString());

        Assert.Equal(str, decomp.ToString());
    }
    [Theory]
    [InlineData("asdfrtgyhuj")]
    public void TestCompressionRuntime3(string str)
    {
        CompressionService.Compression compression = new CompressionService.Compression();
        
        var comp = compression.CompressionMethod(str);
        var decomp = compression.DecompressionMethod(comp.ToString());

        Assert.Equal(str, decomp.ToString());
    }
}